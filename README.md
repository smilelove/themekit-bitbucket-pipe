# Bitbucket Pipelines Pipe: themekit_bitbucket_pipe

**There's a major flaw with this pipe.  It doesn't have access to the git code.  I'm new to writing pipes, so if someone knows how to fix this, I'm happy to have some advice.**

This Bitbucket Pipe is to help manage Shopify sites using Themekit

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml

- pipe: docker://bjtsmilelove/themekit_bitbucket_pipe:0.0.1
  variables:
    SHOPIFY_ENV: '<string>'
    SHOPIFY_THEME_ID: '<string>'
    SHOPIFY_PASSWORD: '<string>'
    SHOPIFY_STORE_URL: '<string>'
    # GIT_RESET: '<boolean>' # This pipe configures the theme.  Reset the configuration file after the Action has been performed?  Good idea to set this to true if you have a configuration file checked in.
    # ACTION: '<string>' # Optional. `deploy` or `download` (defaults to `download`).
    # CONFIGURE_ARGS: '<string>' # Optional.  Additional arguments to pass to 'theme configure'

```

## Variables

| Variable           | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| SHOPIFY_ENV (*) | Name of the environment to use (passed as `--env` to themekit)  |
| SHOPIFY_THEME_ID (*)     | Id of the Shopify Theme |
| SHOPIFY_PASSWORD (*)     | Your password from your private app |
| SHOPIFY_STORE_URL (*)     | Your store url. (e.g. `demo.myshopify.com`). |
| GIT_RESET           | Reset the configuration file after the Action has been performed?  Good idea to set this to true if you have a configuration file checked in. Default: `false`. | 
| ACTION           | `deploy` or `download`. Default: `download`. | 
| CONFIGURE_ARGS           | Additional arguments to pass to 'theme configure'. Optional argument descriptions are available on [Theme Kit help](https://shopify.github.io/themekit/configuration/#flags). Default: ``. | 

_(*) = required variable._

## Details

TODO

## Prerequisites

1) Generate a private app API KEY on Shopify. [Shopify API Access](https://shopify.github.io/themekit/#get-api-access).
2) It's recommended to setup the following secrets\variables in [Bitbucket variables](https://confluence.atlassian.com/bitbucket/variables-in-pipelines-794502608.html).
  * **SHOPIFY_PASSWORD**
  * **SHOPIFY_THEME_ID**

## Examples

```yaml
- pipe: docker://bjtsmilelove/themekit_bitbucket_pipe:0.0.1
  variables:
    SHOPIFY_ENV: 'development'
    SHOPIFY_THEME_ID: $SHOPIFY_THEME_ID
    SHOPIFY_PASSWORD: $SHOPIFY_PASSWORD
    SHOPIFY_STORE_URL: 'demo.myshopify.com'
```
Passing additional optional arguments

```yaml
- pipe: docker://bjtsmilelove/themekit_bitbucket_pipe:0.0.1
  variables:
    SHOPIFY_ENV: 'development'
    SHOPIFY_THEME_ID: $SHOPIFY_THEME_ID
    SHOPIFY_PASSWORD: $SHOPIFY_PASSWORD
    SHOPIFY_STORE_URL: 'demo.myshopify.com'
    GIT_RESET: 'true'
    ACTION: 'deploy'
    CONFIGURE_ARGS: '--ignored-file=README.md --timeout=30'
```

## Support

If you'd like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you're reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License

The Dockerfile and associated scripts and documentation in this project are released under the [MIT License](LICENSE).
