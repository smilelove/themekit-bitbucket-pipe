FROM debian:stable-slim

RUN apt-get update && \
    apt-get install -y curl python && \
    rm -rf /var/lib/apt/lists/*

RUN curl -s https://shopify.github.io/themekit/scripts/install.py | python

COPY "pipe.sh" "/pipe.sh"
RUN chmod +x /pipe.sh

ENTRYPOINT ["/pipe.sh"]
