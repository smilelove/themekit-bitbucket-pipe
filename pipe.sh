#!/bin/sh

GIT_RESET=${GIT_RESET:="false"}
ACTION=${ACTION:="download"}
CONFIGURE_ARGS=${CONFIGURE_ARGS:=""}

theme configure --password=$SHOPIFY_PASSWORD --store=$SHOPIFY_STORE_URL --themeid=$SHOPIFY_THEME_ID --env=$SHOPIFY_ENV $CONFIGURE_ARGS
if [ $ACTION = "deploy" ]; then
    theme deploy --env=$SHOPIFY_ENV
else
    theme download --env=$SHOPIFY_ENV
fi

if [ $GIT_RESET = "true" ]; then
    git reset --hard HEAD && git clean -df
fi